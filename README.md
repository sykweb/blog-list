# SYKWEB evaluation - Blog list #

Test duration: 3 days.

### Before you begin ###
Please create a new branch with your name: eg. for John Smith create a branch called john-smith and do all your work on there.

### Objective ###

You must create a single web page that shows a list of 20 blog posts.

The web page must fetch the posts from this endpoint when it is loaded: http://jsonplaceholder.typicode.com/posts

Details to for each post:

* Title
* Body (text) - limited to 20 words by default
* "Expand" button: when you click on it the full blog post text shows and a button called "Show less" appears (the "Expand" button disappears). When you click on "Show less" the reverse happens: the text is limited to 20 words again and the "Expand" button re-appears. 
* "Random comment" button: When you click this button a random comment for this post appears under the blog text. If you click it again then this comment will be replaced by another comment. All comments must be related to the blog post.
* Comment (a different comment every time the button is clicked), by default there is no comment. Comments are located at http://jsonplaceholder.typicode.com/comments
* The web page must be responsive (design that fits all screen sizes).

Here are some details on the API:

![post_comments.png](https://bitbucket.org/repo/K7Xjkg/images/13404705-post_comments.png)


### Bonus ###

Bonus points for an attractive design.

Bonus points for using React.js or Angular.js


### Tips ###

* `commit` often and with well-written messages.
* `commit` messages can be in French or English
* Use whatever libraries and tools you feel will help your each the objective.
* Web applications must look great as well as function well.
* Quality over quantity, always.

### Help ###
For clarification or other queries contact saemie@chouchane.com

Bonne chance.